<?php

/**
 * the auto-loading function, which will be called every time a file "is missing"
 * NOTE: don't get confused, this is not "__autoload", the now deprecated function
 * The PHP Framework Interoperability Group (@see https://github.com/php-fig/fig-standards) recommends using a
 * standardized auto-loader https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md, so we do:
 */
function autoload($class) {
    // if file does not exist in LIBS_PATH folder [set it in config/config.php]
    if (file_exists(LIBRARY_PATH . "/" . $class . ".php")) {
        require LIBRARY_PATH . "/" . $class . ".php";
    } elseif(file_exists(LIBRARY_PATH . "/core/" . $class . ".php")) {
        require LIBRARY_PATH . "/core/" . $class . ".php";
    } elseif(file_exists(MODEL_PATH . "/" . $class . ".php")) {
        require MODEL_PATH . "/" . $class . ".php";
    } elseif(file_exists(CONTROLLER_PATH . "/" . $class . ".php")) {
        require CONTROLLER_PATH . "/" . $class . ".php";
    } elseif(file_exists(LIB_PATH . "/" . $class . ".php")) { //APP lib path
        require LIB_PATH . "/" . $class . ".php";
    }  
    else {
        //exit ('The file ' . $class . '.php is missing in the libs folder.');
    }
}

// spl_autoload_register defines the function that is called every time a file is missing. as we created this
// function above, every time a file is needed, autoload(THENEEDEDCLASS) is called
spl_autoload_register("autoload");

require LIBRARY_PATH . "/base_functions.php";