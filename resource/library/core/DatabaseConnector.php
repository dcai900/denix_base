<?php

/*
 * Database Connector
 * 
 * Initial database connection for App uses
 */

/**
 * Description of DatabaseConnector
 *
 * @author dcai
 */
class DatabaseConnector {

    protected static $_init = false;
    protected static $_dataSources;
    protected static $_use_pdo = false; //allow to overwrite DB_CONNECTOR

    /**
     * Loads connections configuration.
     *
     * @return void
     */
    protected static function _init() {
        if(self::$_use_pdo){
            require_once(LIBRARY_PATH . "/db/Database.php");
            self::$_dataSources = new Database();
        }
        elseif(DB_CONNECTOR == "classic"){
            require_once(LIBRARY_PATH . "/db/DatabaseClassic.php");
            self::$_dataSources = new DatabaseClassic(DB_HOST, DB_USER, DB_PASS, DB_NAME);
            self::$_dataSources->connect();
        }
        else{
            require_once(LIBRARY_PATH . "/db/DatabasePDO.php");    
            self::$_dataSources = DatabasePDO::getInstance();
        }
        
        self::$_init = true;
    }

    public static function getDataSource($pdo=false) {
        if($pdo){
            self::$_use_pdo = true;
        }
        if (empty(self::$_init)) {
            self::_init();
        }
        return self::$_dataSources;
    }
    
    public static function destory(){
        if(!empty(self::$_init)) {
            self::$_dataSources = NULL;
            self::$_init = false;
            self::$_use_pdo = false;
        }
    }

}
