<?php

/**
 * Session class
 *
 * handles the session stuff. creates session when no one exists, sets and
 * gets values, and closes the session properly (=logout). Those methods
 * are STATIC, which means you can call them with Session::get(XXX);
 */
class Session {

    /**
     * starts the session
     */
    public static function init() {
        //Session Auto Start Control
        $_session_id = session_id();
        if (empty($_session_id)){
            ini_set('session.cookie_httponly',1);
            session_start();
        }
        unset($_session_id);

        //Session Expiration Control
        if (!isset($_SESSION['CREATED'])) {
            $_SESSION['CREATED'] = time();
        } else if (time() - $_SESSION['CREATED'] > SESSION_TIMOUT) {
            // idle more than 10 minutes ago
            session_destroy();
            $_SESSION = array();
        } else {
            $_SESSION['CREATED'] = time();
        }
    }

    /**
     * sets a specific value to a specific key of the session
     * @param mixed $key
     * @param mixed $value
     */
    public static function set($key, $value) {
        $_SESSION[$key] = $value;
    }
    
    public static function add_negative_msg($value) {
        $_SESSION['feedback_negative'][] = $value;
    }
    
    public static function add_positive_msg($value) {
        $_SESSION['feedback_positive'][] = $value;
    }
    

    /**
     * gets/returns the value of a specific key of the session
     * @param mixed $key Usually a string, right ?
     * @return mixed
     */
    public static function get($key) {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        } else {
            return null;
        }
    }

    /**
     * 
     * Delete a session by key
     * @param type $key
     */
    public static function delete($key) {
        if (isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }
    }

    /**
     * deletes the session (= logs the user out)
     */
    public static function destroy() {
        session_destroy();
    }

}
