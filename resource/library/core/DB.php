<?php

/*
 * Database CRUD
 * 
 */

class DB{

    protected $db;
    protected $table;
    protected $columns;

    public function __construct($table = null, $columns = null) {
        $this->db = DatabaseConnector::getDataSource(true); //to use pdo
    }

    public function insertData($data, $table = null, $columns = null) {
        $has_columns = ($columns) ? 1 : 0;
        
        if(!$table){
            $table = $this->table;
        }
        if(!$columns){
            $columns = $this->columns;
        }
        
        $insert_data = array();
        foreach ($columns as $k) {
            if (isset($data[$k])) {
                $insert_data[$k] = $data[$k];
            } else {
                $insert_data[$k] = 0;
            }
        }
        
        $query = "INSERT INTO $table";
        $keys = array_keys($insert_data);
        $keys_str = implode(",", $keys);
        $values_str = ':' . implode(",:", $keys);
        $query .= " (" . $keys_str . ") VALUE (" . $values_str . ")";

        //debug($query);

        foreach ($insert_data as $name => $value) {
            $values[':' . $name] = $value; // save the placeholder
        }

        //debug($values);

        $sth = $this->db->prepare($query);

        $sth->execute($values);
    }

    public function updateData($data, $conditions, $table, $columns) {
        if(!$table){
            $table = $this->table;
        }
        if(!$columns){
            $columns = $this->columns;
        }
        $update_data = array();
        foreach ($this->columns as $k) {
            if (isset($data[$k])) {
                $update_data[$k] = $data[$k];
            }
        }
        
        $query = "UPDATE $this->table SET ";
        $update_arr = array();

        foreach ($update_data as $name => $value) {
            $update_arr[] .= $name . "=:" . $name;
            $values[':' . $name] = $value; // save the placeholder
        }

        $update_str = implode(",", $update_arr);
        $query .= $update_str;
        
        $conditions_arr = array();
        foreach($conditions as $key => $val){
            $conditions_arr[] = $key . "= :" . $key; 
            $values[$key] = $val;
        }
        $conditions_str = implode(" AND ", $conditions_arr);
        
        $query .= ' WHERE ' . $conditions_str;

        $sth = $this->db->prepare($query);

        $sth->execute($values);
    }

    function validateDate($date) {
        $d = DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') == $date;
    }
    
    


}
