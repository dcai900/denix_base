<?php

/**
 * This is the "base controller class". All other "real" controllers extend this class.
 * Whenever a controller is created, we also
 * 1. initialize a session
 * 2. check if the user is not logged in anymore (session timeout) but has a cookie
 * 3. create a database connection (that will be passed to all models that need a database connection)
 * 4. create a view object
 */
class Controller {

    public $controller;
    public $action;
    
    protected $pageRender;
    protected $isAuth;
    protected $isAdmin;

    public function __construct($controller=null,$params=null) {
        Session::init();
        
        if(defined("USE_DENI_AUTH")){
            $this->isAuth = Auth::isAuth();
            $this->isAdmin = Auth::isAdmin();
        }
            
        $this->controller = $controller;
        $this->action = ($params && count($params)) ? $params[0] : null;
        
        //if not login page, then delete back_url session variable
        if($this->controller != 'user' && $this->action != 'login'){
            Session::delete('back_url');
        }
        
        $default_variables = $this->setDefaultNavi();
        
        $default_variables['isAuth'] = $this->isAuth;
        $default_variables['isAdmin'] = $this->isAdmin;
        
        $this->pageRender = new PageRender(
                $this->controller, $default_variables);
        
    }
    
    public function setDefaultNavi(){
        $topnav = $topnav_right = $sidenav = $sidenav_right = null;
        //$navis and $navis_right can be defined in app/lib/Constants.php
        if(isset(Constants::$navis)){
            $topnav = deniXHelper::navi_gen(Constants::$navis, $this->controller);          
        }
        
        if(isset(Constants::$navis_right) && !$this->isAuth){
            $topnav_right = deniXHelper::navi_gen(Constants::$navis_right);
        }
        else if(isset(Constants::$auth_navis_right) && $this->isAuth){
            $topnav_right = deniXHelper::navi_gen(Constants::$auth_navis_right);
        }
        
        if(isset(Constants::$navis_sidebar_left)){
            $sidenav_right = deniXHelper::sidebar_gen(Constants::$navis_sidebar_left, $this->controller);
        }
        
        if(isset(Constants::$navis_sidebar_right)){
            $sidenav_left = deniXHelper::sidebar_gen(Constants::$navis_sidebar_right, $this->controller);
        }
                
        return compact("topnav", "topnav_right","sidenav_left", "sidenav_right");

    }
    

}
