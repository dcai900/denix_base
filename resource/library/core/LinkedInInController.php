<?php

/*
 * hanle home page
 *
 * 
 * 
 */

class LinkedInInController extends Controller {

    private $_params;
    private $api_callback_url;

    public function __construct($controller = null, $params = null) {
        parent::__construct($controller, $params);
        $this->_params = $params;
    }

    public function set_api_callback_url($url){
        $this->api_callback_url = $url;
    }
    
    public function fetch_picture() {        
       $picture = $this->fetch('GET', "/v1/people/~/picture-urls::(original)");
       $picture_url = $picture->values;
       return $picture_url;       
    }
    
    
    public function fetch_certifications() {
        $query = 'id,certifications';
        $profile = $this->fetch('GET', '/v1/people/~:(' . $query . ')');
        return $profile;
    }

    public function fetch_educations() {
        $query = 'id,educations';
        $profile = $this->fetch('GET', '/v1/people/~:(' . $query . ')');
        return $profile;
    }

    public function fetch_skills() {
        $query = 'id,skills';
        $profile = $this->fetch('GET', '/v1/people/~:(' . $query . ')');
        return $profile;
    }

    public function fetch_profile() {
        $query = 'id,firstName,lastName,maidenName,formattedName,phoneticFirstName,phoneticLastName,formattedPhoneticName,headline,location,industry,distance,currentShare,numConnections,summary,specialties,pictureUrl,siteStandardProfileRequest,publicProfileUrl,emailAddress,lastModifiedTimestamp,proposalComments,associations,interests,mfeedRssUrl,following,jobBookmarks,suggestions,dateOfBirth,memberUrlResources,relatedProfileViews,honorsAwards,phoneNumbers,boundAccountTypes,imAccounts,mainAddress,twitterAccounts,primaryTwitterAccount,connections,groupMemberships,network';
        $profile = $this->fetch('GET', '/v1/people/~:(' . $query . ')');
        return $profile;
    }

    public function process() {
        // OAuth 2 Control Flow
        if (isset($_GET['error'])) {
            // LinkedIn returned an error
            print $_GET['error'] . ': ' . $_GET['error_description'];
            exit;
        } elseif (isset($_GET['code'])) {
            // User authorized your application
            if ($_SESSION['state'] == $_GET['state']) {
                // Get token so you can make API calls
                $this->getAccessToken();
            } else {
                // CSRF attack? Or did you mix up your states?
                exit;
            }
        } else {
            if ((empty($_SESSION['expires_at'])) || (time() > $_SESSION['expires_at'])) {
                // Token has expired, clear the state
                $_SESSION = array();
            }
            if (empty($_SESSION['access_token'])) {
                // Start authorization process
                $this->getAuthorizationCode();
            }
        }
    }

    function getAuthorizationCode() {
        
        $redirect_url = ($this->api_callback_url) ? 
                $this->api_callback_url : REDIRECT_URI;
        
        $params = array('response_type' => 'code',
            'client_id' => API_KEY,
            'scope' => SCOPE,
            'state' => uniqid('', true), // unique long string
            'redirect_uri' => $redirect_url,
        );

        // Authentication request
        $url = 'https://www.linkedin.com/uas/oauth2/authorization?' . http_build_query($params);

        // Needed to identify request when it returns to us
        $_SESSION['state'] = $params['state'];

        // Redirect user to authenticate
        header("Location: $url");
        exit;
    }

    function getAccessToken() {
        $redirect_url = ($this->api_callback_url) ? 
                $this->api_callback_url : REDIRECT_URI;
        $params = array('grant_type' => 'authorization_code',
            'client_id' => API_KEY,
            'client_secret' => API_SECRET,
            'code' => $_GET['code'],
            'redirect_uri' => $redirect_url,
        );

        // Access Token request
        $url = 'https://www.linkedin.com/uas/oauth2/accessToken?' . http_build_query($params);

        // Tell streams to make a POST request
        $context = stream_context_create(
                array('http' =>
                    array('method' => 'POST',
                    )
                )
        );

        // Retrieve access token information
        $response = file_get_contents($url, false, $context);

        // Native PHP object, please
        $token = json_decode($response);

        // Store access token and expiration time
        $_SESSION['access_token'] = $token->access_token; // guard this! 
        $_SESSION['expires_in'] = $token->expires_in; // relative time (in seconds)
        $_SESSION['expires_at'] = time() + $_SESSION['expires_in']; // absolute time

        return true;
    }

    function fetch($method, $resource, $body = '') {
        $params = array('oauth2_access_token' => $_SESSION['access_token'],
            'format' => 'json',
        );

        // Need to use HTTPS
        $url = 'https://api.linkedin.com' . $resource . '?' . http_build_query($params);
        // Tell streams to make a (GET, POST, PUT, or DELETE) request
        $context = stream_context_create(
                array('http' =>
                    array('method' => $method,
                    )
                )
        );

        //debug($url);
        // Hocus Pocus
        $response = file_get_contents($url, false, $context);
        //debug($response);
        // Native PHP object, please
        return json_decode($response);
    }

}

?>
