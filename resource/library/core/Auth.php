<?php

/**
 * Class Auth
 * Simply checks if user is logged in. In the app, several controllers use Auth::handleLogin() to
 * check if user if user is logged in, useful to show controllers/methods only to logged-in users.
 */
class Auth {

    public static function handleLogin() {
        // initialize the session
        Session::init();

        // if user is still not logged in, then destroy session, handle user as "not logged in" and
        // redirect user to login page
        if (!isset($_SESSION['user_logged_in'])) {
            Session::destroy();
            header('location: ' . SITE_BASE_URL . '/user/login');
        }
    }
    
    public static function isAuth(){
        if (Session::get('user_logged_in') == true) {
            return TRUE;
        }
        elseif(isset($_COOKIE['rememberme'])) {
            $user = new User();
            return $user->loginWithCookieData(true); //slient login
        }
        else{
            return FALSE;
        }
    }
    public static function isAdmin(){
        return Session::get('user_logged_in') == true 
                && Session::get('user_account_type') == 1;
    }
    
    public static function isAccountManager(){
        return Session::get('user_logged_in') == true 
                && Session::get('user_account_type') == Constants::ACCOUNT_MANAGER_ID;
    }
    
    public static function isMentor(){
        return Session::get('user_logged_in') == true 
                && Session::get('user_account_type') == Constants::MENTOR_ID;
    }
    
    public static function isMentee(){
        return Session::get('user_logged_in') == true 
                && Session::get('user_account_type') == Constants::MENTEE_ID;
    }

    public static function isClientEmployer(){
        return Session::get('user_logged_in') == true 
                && Session::get('user_account_type') == Constants::CLIENT_ID;
    }
    

}
