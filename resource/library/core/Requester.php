<?php

/*
 * Accept Requests from clients
 */

/**
 * Static Requestor
 *
 * @author dcai
 */
class Requester {

    private static $_uri = ''; //from url
    private static $_data = array(); //from post
    private static $_query = array(); //from get
    private static $_initialized = false;
    public static $controller = DEFAULT_CONTROLLER;
    public static $params = array();

    private static function _init() {
        if (self::$_initialized) {
            return;
        }

        self::$_uri = self::_read_uri();
        self::$_data = self::_read_data();
        self::$_query = self::_read_query();

        if (self::$_uri) {
            $uris = preg_split("/\//", self::$_uri);
            $controller = strtolower($uris[0]);
            //$class_name = ucfirst($controller) . "Controller";
            $class_name = get_controller_class_name($controller);

            if (class_exists($class_name)) {
                self::$controller = $controller;
                self::$params = array_slice($uris, 1);
            } else {
                self::$params = $uris;
            }
        }
        self::$_initialized = true;
    }

    public static function get_uri() {
        self::_init();
        return self::$_uri;
    }

    public static function get_referer() {
        if (isset($_SERVER['HTTP_REFERER'])) { // check if referrer is set
            return $_SERVER['HTTP_REFERER'];
        } else {
            return null;
        }
    }

    public static function get_data($k = NULL, $clean = true) {
        self::_init();
        $vals = self::$_data;
        if ($clean) {
            array_walk_recursive($vals, 'self::clean_xss');
        }
        if (!$k) {
            return $vals;
        } elseif (isset($vals[$k])) {
            return $vals[$k];
        } else {
            return null;
        }
    }

    public static function get_query($k = NULL, $clean = true) {
        self::_init();
        $vals = self::$_query;
        if ($clean) {
            array_walk_recursive($vals, 'self::clean_xss');
        }
        if (!$k) {
            return $vals;
        } elseif (isset($vals[$k])) {
            return $vals[$k];
        } else {
            return null;
        }
    }

    /**
     * modified from cakephp's version to get uri.
     * 
     * @return str;
     */
    private static function _read_uri() {
        if (!empty($_SERVER['PATH_INFO'])) {
            return $_SERVER['PATH_INFO'];
        } elseif (isset($_SERVER['REQUEST_URI'])) {
            $uri = $_SERVER['REQUEST_URI'];
        } elseif (isset($_SERVER['PHP_SELF']) && isset($_SERVER['SCRIPT_NAME'])) {
            $uri = str_replace($_SERVER['SCRIPT_NAME'], '', $_SERVER['PHP_SELF']);
        } elseif (isset($_SERVER['HTTP_X_REWRITE_URL'])) {
            $uri = $_SERVER['HTTP_X_REWRITE_URL'];
        } elseif ($var = env('argv')) {
            $uri = $var[0];
        }

        $base = SITE_BASE_URL;

        if (strlen($base) > 0 && strpos($uri, $base) === 0) {
            $uri = substr($uri, strlen($base));
        }
        if (strpos($uri, '?') !== false) {
            list($uri) = explode('?', $uri, 2);
        }
        if (empty($uri) || $uri == '/' || $uri == '//') {
            $uri = '/';
        }

        //trim both "/" around, e.g. return "page/1" instead of "/page/1/"
        $uri = trim($uri, "/");
        //we should clean uri, since the input could be get from uri, 
        //like search/keyword, and keyword is from URI
        return $uri;
        //return strtolower($uri);
    }

    //stripslashes when there is magic_quotes
    private static function _read_data() {
        if (get_magic_quotes_gpc()) {

            function stripslashes_gpc(&$value) {
                $value = stripslashes($value);
            }

            array_walk_recursive($_POST, 'stripslashes_gpc');
        }
        return $_POST;
    }

    //stripslashes when there is magic_quotes
    private static function _read_query() {
        if (get_magic_quotes_gpc()) {

            function stripslashes_gpc(&$value) {
                $value = stripslashes($value);
            }

            array_walk_recursive($_GET, 'stripslashes_gpc');
        }
        return $_GET;
    }

    public static function get_paginate_page() {
        $parts = preg_split('/\//', self::$_uri);
        foreach ($parts as $i => $part) {
            if ($part == PAGINATE_PARAM_PAGE) {
                return isset($parts[$i + 1]) ? intval($parts[$i + 1]) : 1;
            }
        }
        return 1;
    }

    public static function get_paginate_limit() {
        $queries = self::$_query;
        if (isset($queries[PAGINATE_PARAM_LIMIT])) {
            return $queries[PAGINATE_PARAM_LIMIT];
        } else {
            return false;
        }
    }

    public static function get_paginate_order() {
        $queries = self::$_query;
        if (isset($queries[PAGINATE_PARAM_ORDER])) {
            return $queries[PAGINATE_PARAM_ORDER];
        } else {
            return false;
        }
    }

    private static function clean_xss(&$param) {
        if (is_string($param)) {
            $param = self::ms_tiddy($param);
            //$param = htmlentities(strip_tags($param));
            $param = strip_tags($param);
        }
    }

    private static function ms_tiddy($string) {
        $search = array(                // www.fileformat.info/info/unicode/<NUM>/ <NUM> = 2018
            "\xC2\xAB", // « (U+00AB) in UTF-8
            "\xC2\xBB", // » (U+00BB) in UTF-8
            "\xE2\x80\x98", // ‘ (U+2018) in UTF-8
            "\xE2\x80\x99", // ’ (U+2019) in UTF-8
            "\xE2\x80\x9A", // ‚ (U+201A) in UTF-8
            "\xE2\x80\x9B", // ‛ (U+201B) in UTF-8
            "\xE2\x80\x9C", // “ (U+201C) in UTF-8
            "\xE2\x80\x9D", // ” (U+201D) in UTF-8
            "\xE2\x80\x9E", // „ (U+201E) in UTF-8
            "\xE2\x80\x9F", // ‟ (U+201F) in UTF-8
            "\xE2\x80\xB9", // ‹ (U+2039) in UTF-8
            "\xE2\x80\xBA", // › (U+203A) in UTF-8
            "\xE2\x80\x93", // – (U+2013) in UTF-8
            "\xE2\x80\x94", // — (U+2014) in UTF-8
            "\xE2\x80\xA6"  // … (U+2026) in UTF-8
        );

        $replacements = array(
            "<<",
            ">>",
            "'",
            "'",
            "'",
            "'",
            '"',
            '"',
            '"',
            '"',
            "<",
            ">",
            "-",
            "-",
            "..."
        );

        return str_replace($search, $replacements, $string);
    }

}
