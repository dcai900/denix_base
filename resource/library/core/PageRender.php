<?php

/*
 * Render Page to invoke correct layout and view pages
 */

/**
 * 
 *
 * @author dcai
 */
class PageRender {

    public $controller;
    public $action = '';
    private $_variables = array();
    private $_layout = 'default';

    public function __construct($controller, $variables) {
        $this->controller = $controller;
        $this->set($variables);
    }

    public function setController($controller) {
        $this->controller = $controller;
    }

    public function setLayout($layout) {
        $this->_layout = $layout;
    }

    public function setAction($action) {
        $this->action = $action;
    }

    //set variable
    public function set($k, $v = null) {
        if (is_array($k)) {
            foreach ($k as $kk => $vv) {
                 $this->_variables[$kk] = $vv;
            }
        } else {
            $this->_variables[$k] = $v;
        }
    }

    public function get($k) {
        if (isset($this->_variables[$k])) {
            return $this->_variables[$k];
        } else {
            return null;
        }
    }
    
    /**
     *  get a post data by key
     */
    public function getPost($key){
        $postData = Requester::get_data();
        if(isset($postData[$key])){
            return $postData[$key];
        }
        else{
            return null;
        }
    }
    
    /**
     *  get a get (query) data by key
     */
    public function getQuery($key){
        $queryData = Requester::get_query();
        if(isset($queryData[$key])){
            return $queryData[$key];
        }
        else{
            return null;
        }
    }
    
    /**
     *  get a session data by key
     */
    public function getSession($key){
        return SESSION::get($key);
    }

    public function setBreadCrumb($bc_navis) {
        if (is_array($bc_navis)) {
            $breadcrumb = Helper::breadcrumb_gen($bc_navis);
        } else {
            $breadcrumb = null;
        }
        $this->_variables['breadcrumb'] = $breadcrumb;
    }

    public function render($view = null) {
        if (file_exists($view)) {
            include($view);
        } elseif ($this->_layout) {
            include VIEW_PATH . "/layouts/" . $this->_layout . ".php";
        }
    }
    
    public function fetch($view = null){
        $view_path = VIEW_PATH . "/commons/" . $view . "_tpl.php";

        if (file_exists($view)) {
            include($view);
        } elseif (file_exists($view_path)) {
            include ($view_path);
        }
        else{
            echo "";
        }
    
    }
    

    /**
     * renders the feedback messages into the view
     */
    public function renderFeedbackMessages() {
        // get the feedback (they are arrays, to make multiple positive/negative messages possible)
        $feedback_positive = Session::get('feedback_positive');
        $feedback_negative = Session::get('feedback_negative');
        $messages = array();
        if($feedback_positive){
            $messages['success'] = $feedback_positive;
        }
        if($feedback_negative){
            $messages['danger'] = $feedback_negative;
        }
        
        // delete these messages (as they are not needed anymore and we want to avoid to show them twice
        Session::set('feedback_positive', null);
        Session::set('feedback_negative', null);

        return $messages;
    }

}
