<?php

/*
 * Boostrap 3.0 Helper Class
 */

/**
 * Description of Constants
 *
 * @author dcai
 */
class deniXHelper {

    public static function build_link($url, $name, $class = "inline") {
        if ($url && $name) {
            return '<a class="' . $class . '" "href="' . $url . '">' . $name . '</a>';
        } else {
            return '';
        }
    }

    public static function panel_sitebar_gen($arr, $controller) {
        $outHTML = '';
        if(!is_array($arr)){
            return $outHTML;
        }
        foreach ($arr as $k => $v) {
            $outHTML .=
                    '<li>
    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> ' . $k . '<span class="fa arrow"></span></a>';
            $outHTML .=
                    '<ul class="nav nav-second-level">';

            $list = (isset($v['list'])) ? $v['list'] : array();
            
            foreach ($list as $k2 => $v2) {
                $outHTML .=
                        '<li>
            <a href="' . SITE_BASE_URL . "/" . $controller . "/" . $v2 . '">' . $k2 . '</a>
         </li>';
            }
            $outHTML .=
                    '</ul> 
</li>';
        }

        return $outHTML;
    }
    
    public static function make_lable($str) {
        $str = preg_replace("/\_/", " ", $str);
        return ucwords($str);
    }
    
    
    public static function build_paragraphs($ps) {
        $outHTML = '';
        foreach ($ps as $p) {
            $outHTML .= '<p>' . $p . '</p>';
        }
        return $outHTML;
    }

    public static function build_section($label, $id = "", $introductions = array()) {
        $output = '<section><h3 id="' . $id . '">' . $label . '</h3></section>';
        foreach ($introductions as $intro) {
            $output .= '<p>' . $intro . '</p>';
        }
        return $output;
    }

    public static function build_upload($name, $label, $id = '', $help = "") {
        if (!$label) {
            $label = Helper::make_lable($name);
        }
        if (!$id) {
            $id = $name;
        }
        $output = <<<EOD
    <div class="form-group">
        <label for="$name">$label</label>
        <input type="file" name="$name" id="$id">
        <p class="help-block">$help</p>
    </div>
    
EOD;
        return $output;
    }

    //build text input
    public static function build_text($name, $value = '', $label = "", $id = '', $class = "col-lg-12", $placeholder = '', $help = "", $data_type = "") {
        if (!$label) {
            $label = Helper::make_lable($name);
        }
        if (!$id) {
            $id = $name;
        }
        if (!$class) {
            $class = 'col-lg-12';
        }

        $output = <<<EOD
        <div class="form-group">

                <div class="row">
                <div class="$class">
          <input id="$name" name="$name" type="text" value="$value" data_type="$data_type" placeholder="$placeholder" class="form-control">
                </div></div>
          <p class="help-block">$help</p>
        </div>
EOD;

        return $output;
    }

    public static function build_textarea($name, $value = '', $label = "", $id = '', $class = "input", $placeholder = '', $help = "") {

        if (!$label) {
            $label = Helper::make_lable($name);
        }
        if (!$id) {
            $id = $name;
        }

        $output = <<<EOD
        <div class="form-group">
            <label for="$name">$label</label>
            <textarea id="$name" name="$name" row="6" class="form-control $class">$value</textarea>
        </div>
EOD;
        return $output;
    }

    //build radio input
    public static function build_radio($name, $values, $selected, $label = "", $id = '', $class = "radio", $placeholder = '', $help = "") {
        if (!$label) {
            $label = Helper::make_lable($name);
        }
        if (!$id) {
            $id = $name;
        }

        $options = '';
        
        foreach ($values as $value => $olabel) {

            $oname = $name . "-" . $value;
            $oid = $id . "-" . $value;
           

            //if the value is array, such as 'Other, please specify', then add other_field next to it
            /**
             *  [value] => Array
                (
                    [brilent_notification] => Brilent Notification List
                    [friends] => Friends
                    [Ads] => Ads
                    [other] => Array
                        (
                            [0] => Other. Please specify:
                            [1] => deniapps
                        )

                )
             */
            $a_type = 'input';
            $a_name = $name . "_other";
            
            
            if (is_array($olabel)) {

                $other_field = $olabel[1];
                $a_type = isset($olabel[2]) ? $olabel[2] : "input"; //field type, textarea, or input by default

                
                $a_name = isset($olabel[3]) ? $olabel[3] : $name . "_other";
                $olabel = $olabel[0];
                
            }
            
            $checked = ($value == $selected) ? ' checked="checked"' : "";
            

            $options .= '<div class="' . $class . '">' .
                    '<label for="' . $oname . '">
                        <input type="radio" name="' . $name . '" id="' . $oid . '" 
                        value="' . $value . '"' . $checked . '>' .
                    $olabel .
                    '</label> </div>';

            if (isset($other_field)) {
                $options .= '<div class="' . $class . '">';
                if($a_type == 'textarea'){                        
                    $options .=   '<textarea name="' . $a_name . '" row="4" id="' . $oid . '-other" class="form-control">' . 
                       $other_field . '</textarea>';
                }
                else{
                    $options .=   '<input type="text" name="' . $a_name . '" id="' . $oid . '-other" 
                        value="' . $other_field . '">';
                }
                    $options .=
                        '</label></div>';
            }
        }

        $output = <<<EOD
   <div class="form-group">
    <label class="control-label" for="$name">$label</label>

      $options
    </div>        
    
EOD;
        return $output;
    }

    public static function build_checkbox($name, $values = array(), $selected = array(), $label = "", $id = '', $class = "checkbox", $placeholder = '', $help = "") {
        if (!$label) {
            $label = Helper::make_lable($name);
        }
        if (!$id) {
            $id = $name;
        }

        $options = '';
        foreach ($values as $value => $olabel) {
            $oname = $name . "-" . $value;
            $oid = $id . "-" . $value;
            

            //if the label is array, such as 'Other, please specify', then add other_field next to it
            if (is_array($olabel)) {
                $other_field = $olabel[1];
                $olabel = $olabel[0];
            }

            $checked = (count($selected) && in_array($value, $selected)) ? ' checked="checked"' : "";
            
            $options .= '<div class="' . $class . '">' .
                    '<label for="' . $oname . '">
                        <input type="checkbox" name="' . $name . '[]" id="' . $oid . '" 
                        value="' . $value . '"' . $checked . '>' .
                    $olabel .
                    '</label></div>';

            if (isset($other_field)) {
                $options .= '<div class="' . $class . '">' .
                        '<input type="text" name="' . $name . '_other" id="' . $oid . '-other" 
                        value="' . $other_field . '">' .
                        '</label></div>';
            }
        }

        $output = <<<EOD
   <div class="form-group">
    <label class="control-label" for="$name">$label</label>
      $options
    </div>        
    
EOD;
        return $output;
    }

    public static function build_multiselect($name, $values = array(), $selected = array(), $label = "", $id = '', $class = "multi-select", $help = "") {
        if (!$label) {
            $label = Helper::make_lable($name);
        }
        if (!$id) {
            $id = $name;
        }

        $options = '';
        foreach ($values as $v => $n) {
            $checked = (in_array($v, $selected)) ? ' selected="selected"' : "";
            $options .= '<option value="' . $v . '"' . $checked . '>' . $n . '</option>';
        }

        $output = <<<EOD
   <div class="form-group">
    <label class="control-label" for="$name">$label</label>
        <select multiple="multiple" name="$name" class="form-control $class">         
        $options
        </select>
    </div>        
    
EOD;
        return $output;
    }

    public static function build_select($name, $values = array(), $selected, $label = "", $id = '', $class = "", $help = "") {
        if (!$label) {
            $label = Helper::make_lable($name);
        }
        if (!$id) {
            $id = $name;
        }

        $options = '';
        foreach ($values as $v => $n) {
            $checked = ($v == $selected) ? ' selected="selected"' : "";
            $options .= '<option value="' . $v . '"' . $checked . '>' . $n . '</option>';
        }

        $output = <<<EOD
   <div class="form-group">
    <label class="control-label" for="$name">$label</label>
        <select name="$name" class="form-control $class">         
        $options
        </select>
    </div>        
    
EOD;
        return $output;
    }

    public static function build_messages_dropdown($messages, $controller) {
        if (!count($messages)) {
            $lines = '<li><div class="left-buffer">No Messages</div></li>';
        } else {
            $lines = '';
            foreach ($messages as $message) {
                $subject = $message['message_subject'];
                $body = substr($message['message_body'], 0, 50) . "...";
                $send_date = date("M d", strtotime($message['send_time']));
                $mid = $message['message_id'];
                $isread = $message['is_read'];
                $sender = $message['sender'];
                if (!$isread) {
                    $new_html = '<span class="badge alert-newmsg">new</span>';
                } else {
                    $new_html = '';
                }
                //$isnew = ($message['read_time']) ? true : false;

                $lines .= <<<EOD
<li>
                        <a href="/$controller/mail/messages/$mid">
                            <div>
                                $sender<br />
                                <strong>$subject</strong>
                                $new_html
                                <span class="pull-right text-muted">
                                    <em>$send_date</em>
                                </span>
                            </div>
                            <div>$body</div>
                        </a>
                    </li>
                    <li class="divider"></li>
EOD;
            }

            $lines .= '<li>
                            <a class="text-center" href="/' . $controller . '/mail/messages">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>';
        }
        $output = <<<EOD
                <ul class="dropdown-menu dropdown-messages">
                    $lines    
                </ul>
 
EOD;
        return $output;
    }

    /**
     * Generate Bootstrap3 style breadcrumb
     * 
     * @param array $arr
     * @return string
     */
    public static function breadcrumb_gen($arr) {
        $out = '<ol class="breadcrumb">';
        foreach ($arr as $k => $v) {
            if ($v) {
                $out .= '<li><a href="' . $v . '">' . $k . '</a></li>';
            } else {
                $out .= '<li class="active">' . $k . '</li>';
            }
        }
        return $out;
    }

    public static function sidebar_gen($navis, $controller = ''){
        $output = self::navi_gen($navis, $controller, '', true);        
        return $output;
    }
    
    /**
     * Generate Bootstrap3 Style fixed top navigation
     * 
     * @param array $navis
     * @param string $controller
     * @param string $prefix
     * @return string
     * 
     * $navis is predefined array with all categories, e.g.
     *   $navis = array(
     *   "IDE" => array("controller" => "ide"),
     *   "Bootstrap" => array("controller" => "bootstrap"),
     *   "PHP" => array("controller" => "php")
     *   );
     * 
     * $controller is current controller, so the list is marked as "active"
     * 
     * $prefix is used when the controller is shared by different mode, for example:
     * $prefix = 'admin'; 
     * 
     */
    public static function navi_gen($navis, $controller = '', $prefix = '', $sidebar=false) {
        
        //if sidebar, then open dropdown menu and indent under parent
        $ulClass= $sidebar ? "indent-ul" : "dropdown";
        $hasToggle = false;
        
        //debug($controller);
        $output = '';
        foreach ($navis as $name => $navi) {
            $d_urls = array();
            $active = '';
            
            $navi_controller = isset($navi['controller']) ? $navi['controller'] : null;
            if ($controller == $navi_controller) {
                $active = ' class="active"';
            }
            
            if (is_int($name)) {
                $output .= '<li><p class="navbar-text navbar-right">' . $navi .
                        '</p></li>';
            } else {
                if (is_array($navi)) {
                    if (isset($navi['dropdown'])) {

                        $d_urls['parent'] = self::navi_url_gen($navi, $controller, $prefix);
                        $d_urls['child'] = array();
                        foreach ($navi['dropdown'] as $n => $subnavi) {
                            $d_urls['child'][$n] = self::navi_url_gen($subnavi, $controller, $prefix);
                        }
                    } else {
                        $url = self::navi_url_gen($navi);
                    }
                } else {
                    $url = $navi;
                }

                if (count($d_urls)) {
                    
                    if($hasToggle){
                        $linkAtt = ' class="dropdown-toggle" data-toggle="dropdown"';
                    }
                    else{
                        $linkAtt = $active;                        
                    }
                    
                    $output .= '<li class="' . $ulClass .'">
                            <a' . $linkAtt . ' href="' .
                            $d_urls['parent'] . '">' . $name .
                            '<span class="caret"></span></a>';

                    $output .= '<ul class="' . $ulClass . '-menu">';
                    foreach ($d_urls['child'] as $k => $v) {
                        $output .= '<li>' . '<a href="' . $v . '">' . $k . '</a></li>';
                    }

                    $output .= '</ul></li>';
                } else {
                    $output .= "<li" . $active . ">" . '<a href="' . $url . '">' . $name
                            . '</a></li>';
                }
            }
        }
        return $output;
    }

    public static function navi_url_gen($navi, $controller = null, $prefix = null) {
        if (is_array($navi)) {
            
            $navi_action = isset($navi['action']) ? $navi['action'] : null;

            if ($prefix) {
                $base = SITE_BASE_URL . "/" . $prefix;
            } else {
                $base = SITE_BASE_URL;
            }
            $url = $base;
            $navi_controller = isset($navi['controller']) ? $navi['controller'] : null;
            if ($navi_controller) {
                $url .= "/" . $navi_controller;
            }
            if ($navi_action) {
                $url .= "/" . $navi_action;
            }

            if (isset($navi['params'])) {
                $navi_params = to_array($navi['params']);
                foreach ($navi_params as $navi_param) {
                    $url .= "/" . $navi_param;
                }
            }
        } else {
            $url = $navi;
        }
        
        return $url;
    }

    /**
     * Modfied from someone's version to support Bootstrap 3 style 
     * (Sorry forgot where I got this)
     * 
     * I'm big fan of Friendly url, so no query string supported, 
     * e.g. /page/1 instead of /?page=1
     *  
     * @param int $page
     * @param type $total_pages
     * @param type $limit
     * @param type $targetpage
     * @param type $stages
     * @param type $param
     * @return string
     */
    public static function paginate_gen($page, $total_pages, $limit, $targetpage, $stages, $param = 'page') {

        // Initial page num setup
        if ($page == 0) {
            $page = 1;
        }
        $prev = $page - 1;
        $next = $page + 1;
        $lastpage = ceil($total_pages / $limit);
        $LastPagem1 = $lastpage - 1;


        $paginate = '';
        if ($lastpage > 1) {

            $paginate .= "<div>";
            $paginate .= '<ul class="pagination">';
            // Previous
            if ($page > 1) {
                $paginate.= '<li><a href="' . $targetpage . '/' . $param . '/$prev">&larr; prev</a></li>';
            } else {
                $paginate.= '<li class="disable"><span>&larr; prev</span></li>';
            }

            // Pages	
            if ($lastpage < 7 + ($stages * 2)) { // Not enough pages to breaking it up
                for ($counter = 1; $counter <= $lastpage; $counter++) {
                    if ($counter == $page) {
                        $paginate.= '<li class="active"><span>' . $counter . '</span></li>';
                    } else {
                        $paginate.= '<li><a href="' . $targetpage . '/' . $param . '/' . $counter . '">' . $counter . '</a></li>';
                    }
                }
            } elseif ($lastpage > 5 + ($stages * 2)) { // Enough pages to hide a few?
                // Beginning only hide later pages
                if ($page < 1 + ($stages * 2)) {
                    for ($counter = 1; $counter < 4 + ($stages * 2); $counter++) {
                        if ($counter == $page) {
                            $paginate.= '<li class="active"><span>' . $counter . '</span></li>';
                        } else {
                            $paginate.= '<li><a href="' . $targetpage . '/' . $param . '/' . $counter . '">' . $counter . '</a></li>';
                        }
                    }
                    $paginate.= '<li><a href="#">...</a></li>';
                    $paginate.= '<li><a href="' . $targetpage . '/' . $param . '/' . $LastPagem1 . '">' . $LastPagem1 . '</a></li>';
                    $paginate.= '<li><a href="' . $targetpage . '/' . $param . '/' . $lastpage . '">' . $lastpage . '</a></li>';
                }
                // Middle hide some front and some back
                elseif ($lastpage - ($stages * 2) > $page && $page > ($stages * 2)) {
                    $paginate.= '<li><a href="' . $targetpage . '/' . $param . '/1">1</a></li>';
                    $paginate.= '<li><a href="' . $targetpage . '/' . $param . '/2">2</a></li>';
                    $paginate.= '<li><a href="#">...</a></li>';
                    for ($counter = $page - $stages; $counter <= $page + $stages; $counter++) {
                        if ($counter == $page) {
                            $paginate.= '<li class="active"></span>' . $counter . '</span></li>';
                        } else {
                            $paginate.= '<li><a href="' . $targetpage . '/' . $param . '/' . $counter . '">' . $counter . '</a></li>';
                        }
                    }
                    $paginate.= '<li><a href="#">...</a></li>';
                    $paginate.= '<li><a href="' . $targetpage . '/' . $param . '/' . $LastPagem1 . '">$LastPagem1</a></li>';
                    $paginate.= '<li><a href="' . $targetpage . '/' . $param . '/' . $lastpage . '">$lastpage</a></li>';
                }
                // End only hide early pages
                else {
                    $paginate.= '<li><a href="' . $targetpage . '/' . $param . '/1">1</a></li>';
                    $paginate.= '<li><a href="' . $targetpage . '/' . $param . '/2">2</a></li>';
                    $paginate.= '<li><a href="#">...</a>';
                    for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++) {
                        if ($counter == $page) {
                            $paginate.= '<li class="active"><span>' . $counter . '</span></li>';
                        } else {
                            $paginate.= '<li><a href="' . $targetpage . '/' . $param . '/' . $counter . '">' . $counter . '</a></li>';
                        }
                    }
                }
            }

            // Next
            if ($page < $counter - 1) {
                $paginate.= '<li><a href="' . $targetpage . '/' . $param . '/' . $next . '">next &rarr; </a></li>';
            } else {
                $paginate.= '<li class="disabled"><span>next &rarr;</span></li>';
            }

            $paginate.= '</ul>';
            $paginate.= '</div>';
        }
        return $paginate;
    }
    
    
    public static function upload($field_name = "file", $file_size = "2000000", $file_name=false, $upload_to=false, $allowedType=false) {
        if(!$upload_to){
            $upload_to = ROOT_PATH . CV_PATH;
        }
        if (!$allowedType) {
            $allowedType = array(
                'jpg' => 'image/jpeg',
                'png' => 'image/png',
                'gif' => 'image/gif',
                'pdf' => array(
                    'application/pdf',
                    'application/x-pdf',
                    'application/acrobat',
                    'applications/vnd.pdf',
                    'text/pdf',
                    'text/x-pdf'
                ),
                'doc' => 'application/msword',
                'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'txt' => 'text/plain'
            );
        }

        try {

            // Undefined | Multiple Files | $_FILES Corruption Attack
            // If this request falls under any of them, treat it invalid.
            if (
                    !isset($_FILES[$field_name]['error']) ||
                    is_array($_FILES[$field_name]['error'])
            ) {
                throw new RuntimeException('Invalid parameters.');
            }

            // Check $_FILES[$field_name]['error'] value.
            switch ($_FILES[$field_name]['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            // You should also check filesize here. 
            if ($_FILES[$field_name]['size'] > $file_size) {
                throw new RuntimeException('Exceeded filesize limit.');
            }

            // DO NOT TRUST $_FILES[$field_name]['mime'] VALUE !!
            // Check MIME Type by yourself.
            $finfo = new finfo(FILEINFO_MIME_TYPE);
            if (false === $ext = recursive_array_search(
                    $finfo->file($_FILES[$field_name]['tmp_name']), $allowedType, true
                    )) {
                throw new RuntimeException('Invalid file format.');
            }

            // You should name it uniquely.
            // DO NOT USE $_FILES[$field_name]['name'] WITHOUT ANY VALIDATION !!
            // On this example, obtain safe unique name from its binary data.
            if(!$file_name){
                $file_name = sha1_file($_FILES[$field_name]['tmp_name']). "_" . time();
            }
            $file_name .= "." . $ext;           
            $upload_file_with_path = $upload_to . $file_name;
 
            if (!move_uploaded_file(
                            $_FILES[$field_name]['tmp_name'], $upload_file_with_path
                    )) {
                throw new RuntimeException('Failed to move uploaded file.');
            }
            return $file_name;
        } catch (RuntimeException $e) {

            return "ERR:" . $e->getMessage();
        }
    }
    
    
    public static function sendMail($to, $from, $from_name, $subject, $body, $format="text", $cc=array()) {
        if(!is_array($to)){
            $to = array($to);
        }
        if(!is_array($cc) && $cc){
            $cc = array($cc);
        }

        // create PHPMailer object (this is easily possible as we auto-load the according class(es) via composer)
        $mail = new PHPMailer;

        // please look into the config/config.php for much more info on how to use this!
        if (EMAIL_USE_SMTP) {
            // set PHPMailer to use SMTP
            $mail->IsSMTP();
            // useful for debugging, shows full SMTP errors, config this in config/config.php
            $mail->SMTPDebug = PHPMAILER_DEBUG_MODE;
            // enable SMTP authentication
            $mail->SMTPAuth = EMAIL_SMTP_AUTH;
            // enable encryption, usually SSL/TLS
            if (defined(EMAIL_SMTP_ENCRYPTION)) {
                $mail->SMTPSecure = EMAIL_SMTP_ENCRYPTION;
            }
            // set SMTP provider's credentials
            $mail->Host = EMAIL_SMTP_HOST;
            $mail->Username = EMAIL_SMTP_USERNAME;
            $mail->Password = EMAIL_SMTP_PASSWORD;
            $mail->Port = EMAIL_SMTP_PORT;
        } else {
            $mail->IsMail();
        }
        
        if($format == 'html'){
            $mail->IsHTML(true);
        }

        // fill mail with data
        $mail->From = $from;
        $mail->FromName = $from_name;

        foreach($to as $t){
        $mail->AddAddress($t);
        }
        foreach($cc as $c){
            $mail->AddCC($c);
        }

        $mail->Subject = $subject;
        $mail->Body = $body;

        return $mail->Send();
    }
    

}
