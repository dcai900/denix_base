<?php

function debug($array) {
    echo "<pre>";
    $traces = debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
    print_r($array);
    echo "</pre>";
}

function not_found() {
    header('HTTP/1.0 404 Not Found');
    header('location: ' . SITE_BASE_URL . "/not-found");
    exit;
}

function redirect($url, $permanently = false) {
    if ($permanently) {
        header("HTTP/1.1 301 Moved Permanently");
    }
    if (substr($url, 0, 4) !== 'http') {
        $redirected_url = SITE_BASE_URL;
        if(substr($url, 0, 1) !== "/"){
            $redirected_url .= "/";
        }
        $redirected_url .= $url;
    }
    else{
        $redirected_url = $url;
    }
    
    header("Location: $redirected_url");
    exit;
}

function pr_pre($array) {
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}

function get_controller_class_name($controller) {
    $arrs = preg_split("/[-_]/", $controller);
    $class_name = '';
    foreach ($arrs as $arr) {
        $class_name .= ucfirst($arr);
    }
    $class_name .= "Controller";
    return $class_name;
}

function get_method_name($action) {
    $arrs = preg_split("/[-_]/", $action);
    $method_name = '';
    if(!$arrs[0]){ //start with underline is not allowed, like _about
        return false;
    }
    foreach ($arrs as $i => $arr) {
        if ($i) {
            $method_name .= ucfirst($arr);
        } else {
            $method_name .= strtolower($arr);
        }
    }
    return $method_name;
}

function get_seo_name($string) {
    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
    $string = strtolower($string);
    //Strip any unwanted characters
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //remove 's, 're, (eg, you're, I'm)
    $string = preg_replace("/('.*?)\s/", " ", $string);
    //Clean multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
}

function link_gen($href, $name, $attributes = array()) {
    $link = '<a href="' . $href . '"';
    foreach ($attributes as $k => $v) {
        $link .= ' ' . $k . '=' . $v;
    }
    $link .= '>' . $name . '</a>';
    return $link;
}

function navi_gen($navis, $controller = '', $prefix = '') {
    $output = '';
    foreach ($navis as $name => $navi) {
        $d_urls = array();
        $active = '';
        if (is_int($name)) {
            $output .= '<li><p class="navbar-text navbar-right">' . $navi .
                    '</p></li>';
        } else {
            if (is_array($navi)) {
                if (isset($navi['dropdown'])) {

                    $d_urls['parent'] = navi_url_gen($navi, $controller, $prefix);
                    $d_urls['child'] = array();
                    foreach ($navi['dropdown'] as $n => $subnavi) {
                        $d_urls['child'][$n] = navi_url_gen($subnavi, $controller, $prefix);
                    }
                } else {
                    $url = navi_url_gen($navi);
                }
            } else {
                $url = $navi;
            }

            if (count($d_urls)) {
                $output .= '<li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="' .
                        $d_urls['parent'] . '">' . $name .
                        '<span class="caret"></span></a>';

                $output .= '<ul class="dropdown-menu">';
                foreach ($d_urls['child'] as $k => $v) {
                    $output .= '<li>' . '<a href="' . $v . '">' . $k . '</a></li>';
                }

                $output .= '</ul></li>';
            } else {
                $output .= "<li" . $active . ">" . '<a href="' . $url . '">' . $name
                        . '</a></li>';
            }
        }
    }
    return $output;
}

function navi_url_gen($navi, $controller = null, $prefix = null) {
    if (is_array($navi)) {
        $navi_controller = isset($navi['controller']) ? $navi['controller'] : null;
        if ($controller == $navi_controller) {
            $active = ' class="active"';
        }
        $navi_action = isset($navi['action']) ? $navi['action'] : null;

        if ($prefix) {
            $base = SITE_BASE_URL . "/" . $prefix;
        } else {
            $base = SITE_BASE_URL;
        }
        $url = $base;
        if ($navi_controller) {
            $url .= "/" . $navi_controller;
        }
        if ($navi_action) {
            $url .= "/" . $navi_action;
        }

        if (isset($navi['params'])) {
            $navi_params = to_array($navi['params']);
            foreach ($navi_params as $navi_param) {
                $url .= "/" . $navi_param;
            }
        }
    } else {
        $url = $navi;
    }
    return $url;
}


/**
 * convert to array if it's string
 * 
 * @param type $input
 * @return type
 */
function to_array($input) {
    if (is_array($input)) {
        return $input;
    } else {
        return array($input);
    }
}

function array_to_ul($array) {
    if (is_array($array)) {
        $out = "<ul>";
        foreach ($array as $key => $elem) {
            if (!is_array($elem)) {
                $out = $out . "<li>$elem</li>";
            } else
                $out = $out . "<li>$key" . array_to_ul($elem) . "</li>";
        }
        $out = $out . "</ul>";
    }
    else {
        $out = $array;
    }
    return $out;
}



?>